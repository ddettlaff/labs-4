﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;

namespace Lab4.Contract
{
    public interface ICzujnik
    {
        void WlaczGrzanie();
        string PodajTemperature();
    }
}
