﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component2;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Grzejnik : AbstractComponent
    {
        ICzujnik czujnik;

        public Grzejnik()
        {
            RegisterRequiredInterface<ICzujnik>();
        }
        public Grzejnik(ICzujnik czujnik)
        { this.czujnik = czujnik; RegisterProvidedInterface<ICzujnik>(this.czujnik); }

        public void Wywolanie()
        {
            Console.WriteLine("jakas metoda");
        }

        public override void InjectInterface(Type type, object impl)
        {
           
        }
    }
}
