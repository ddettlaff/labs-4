﻿using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;

namespace Lab4.Component2B
{
    public class CzujnikB : AbstractComponent, ICzujnik
    {
        public CzujnikB()
        { RegisterProvidedInterface<ICzujnik>(this); }

        public void Metoda2B()
        {
            Console.WriteLine("metoda z komponentu 2b");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        public void WlaczGrzanie()
        {
            throw new NotImplementedException();
        }

        public string PodajTemperature()
        {
            throw new NotImplementedException();
        }
    }
}
