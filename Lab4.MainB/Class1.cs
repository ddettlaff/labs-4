﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4;
using Lab4.Component2B;
using Lab4.Contract;
using Lab4.Component2;

namespace Lab4.MainB
{
    public class Class1
    {
        public void MetodaMainB()
        {
        Czujnik czujnik = new Czujnik();
        czujnik.WlaczGrzanie();

        Container kontener = new Container();
        CzujnikB czujnik1 = new CzujnikB();
        czujnik.RegisterProvidedInterface<ICzujnik>(czujnik1);
        kontener.RegisterComponent(czujnik1);
        }
        
    }
}
