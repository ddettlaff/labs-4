﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2
{
    public class Czujnik : AbstractComponent, ICzujnik
    {

        public Czujnik()
        {
            RegisterProvidedInterface<ICzujnik>(this);
        }

        public string PodajTemperature()
        {
            return "Aktualna temperatura";
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        public void WlaczGrzanie()
        {
            throw new NotImplementedException();
        }
    }
}
