﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component2;
using Lab4.Component1;
using Lab4.Contract;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Grzejnik grzejnik = new Grzejnik();
            grzejnik.Wywolanie();

            Container kontener = new Container();
            Czujnik czujnik = new Czujnik();
            czujnik.RegisterProvidedInterface<ICzujnik>(czujnik);
            kontener.RegisterComponent(czujnik);

            kontener.RegisterComponents(czujnik, grzejnik);

            Console.ReadLine();
        }
    }
}
